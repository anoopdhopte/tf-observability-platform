variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
}

variable "PROJECT" {
  description = "Provide name of the project"
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "testing"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "OFFICE_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of lodz office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "CI_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of CI"
  default     = ["18.192.228.102/32", "18.158.189.113/32", "18.192.153.221/32", "18.193.144.231/32"]
  type        = list(string)
}

variable "EXTRA_CIDR_BLOCKS" {
  description = "Provide extra CIDR blocks"
  default     = []
  type        = list(string)
}

variable "DOCKER_REGISTRY" {
  description = "Docker registry"
  default     = "docker-release.otlabs.fr"
}

variable "REGISTRY_PATH" {
  description = "Thanos docker registry path"
  default     = "infra"
}

variable "THANOS_DOCKER_IMAGE" {
  description = "Thanos docker image name"
  default     = "docker-thanos"
}

variable "THANOS_VERSION" {
  description = "Version of Thanos module"
  default     = "0.15.0-20201002"
}

variable "ADMIN_ROLES_ARN" {
  description = "Roles which should have access to kubernetes API"
  type        = list(string)
}

variable "PROMETHEUS_EXTERNAL_LABEL" {
  description = "Prometheus label used to thanos connection"
  type        = string
}

variable "KUBERNETES_VERSION" {
  description = "Version of kubernetes which should be deployed"
  default     = "1.17"
  type        = string
}

variable "COGNITO_API_SECRET" {
  type        = string
  default     = ""
  description = "App api key secret for integration with corporate cognito"
}

variable "FEDERATED_COGNITO_DOMAIN" {
  type        = string
  default     = ""
  description = "Federated cognito public url"
}

variable "USER_POOL_ID" {
  type        = string
  default     = ""
  description = "User pool id"
}

variable "ISSUER_URL" {
  type        = string
  default     = ""
  description = "Issuer url"
}

variable "ES_VERSION" {
  description = "Elasticsearch version"
  type        = string
  default     = "7.7"
}

variable "ES_NODE_TO_NODE_ENCRYPTION" {
  description = "Whether to enable node-to-node encryption"
  type        = bool
  default     = true
}
variable "ES_ADVACED_SECURITY_ENABLED" {
  description = "Enable advance security option"
  type        = bool
  default     = true
}

variable "ES_INTERNAL_DATABASE_ENABLED" {
  description = "Create internal Kibana user database"
  type        = bool
  default     = false
}

variable "ES_MASTER_USER_ARN" {
  description = "Master user arn"
  type        = string
  default     = ""
}

variable "ES_MASTER_USER_NAME" {
  description = "Master user name"
  type        = string
  default     = null
}

variable "ES_MASTER_USER_PASS" {
  description = "Master user name"
  type        = string
  default     = null
}

variable "ES_INSTANCE_TYPE" {
  description = "Instance type for ES nodes"
  type        = string
  default     = "r5.large.elasticsearch"
}

variable "ES_INSTANCE_COUNT" {
  description = "Number of instances in the cluster"
  type        = number
  default     = 3
}

variable "ES_MASTER_ENABLED" {
  description = "Indicates whether to use a dedicated master node for the Amazon ES domain"
  type        = bool
  default     = true
}

variable "ES_MASTER_COUNT" {
  description = "The number of instances to use for the master node"
  type        = number
  default     = 3
}

variable "ES_MASTER_TYPE" {
  description = "The hardware configuration of the computer that hosts the dedicated master node"
  type        = string
  default     = "r5.large.elasticsearch"
}

variable "ES_ZONE_AWARENESS" {
  description = "Indicates whether to enable zone awareness for the Amazon ES domain"
  type        = bool
  default     = false
}

variable "ES_ZONE_COUNT" {
  description = "Number of Availability Zones for the domain (Use only if Awareness is enabled)"
  type        = number
  default     = 2
}

variable "ES_ENCRYPT_AT_REST" {
  description = "Enable encryption at rest (only specific instance family types support it: m4, c4, r4, i2, i3 default: false)"
  type        = bool
  default     = false
}

variable "ES_KMS_KEY" {
  description = "KMS key id for elasticsearch"
  type        = string
  default     = ""
}

variable "EBS_ENABLED" {
  description = "Whether EBS volumes are attached to data nodes in the domain"
  type        = bool
  default     = true
}

variable "EBS_VOLUME_SIZE" {
  description = "The size (in GiB) of the EBS volume for each data node. Required if ebs_enabled is set to true"
  type        = number
  default     = 10
}

variable "EBS_VOLUME_TYPE" {
  description = "The EBS volume type to use with the Amazon ES domain, such as standard, gp2, io1, st1, or sc1"
  type        = string
  default     = "gp2"
}

variable "EBS_IOPS" {
  description = "The number of I/O operations per second (IOPS) that the volume supports. This property applies only to the Provisioned IOPS (SSD) EBS volume type"
  type        = number
  default     = 0
}

variable "ES_DOMAIN_ENFORCE_HTTPS" {
  description = "Whether or not to require HTTPS"
  type        = bool
  default     = false
}

variable "ES_DOMAIN_SECURITY_POLICY" {
  description = "The name of the TLS security policy that needs to be applied to the HTTPS endpoint. Valid values: `Policy-Min-TLS-1-0-2019-07` and `Policy-Min-TLS-1-2-2019-07`"
  type        = string
  default     = "Policy-Min-TLS-1-2-2019-07"
}

variable "ES_SG_ID" {
  description = "List of VPC Security Group IDs to be applied to the Elasticsearch domain endpoints. If omitted, the default Security Group for the VPC will be used"
  type        = list(any)
  default     = []
}

variable "ES_SUBNET_ID" {
  description = "List of VPC Subnet IDs for the Elasticsearch domain endpoints to be created in"
  type        = list(any)
  default     = []
}

variable "ES_VPC_ACCESS" {
  description = "Set to true to run ES domain in VPC access mode"
  type        = bool
  default     = false
}

variable "ES_LOGS_ENABLED" {
  description = "Specifies whether given log publishing option is enabled or not"
  type        = bool
  default     = false
}

variable "ES_LOG_TYPE" {
  description = "A type of Elasticsearch log. Valid values: INDEX_SLOW_LOGS, SEARCH_SLOW_LOGS, ES_APPLICATION_LOGS"
  type        = string
  default     = "INDEX_SLOW_LOGS"
}

variable "ES_CLOUDWATCH_LOG_GROUP" {
  description = "ARN of the Cloudwatch log group to which log needs to be published"
  type        = string
  default     = ""
}

variable "ES_COGNITO_ENABLED" {
  description = "Specifies whether Amazon Cognito authentication with Kibana is enabled or not"
  type        = bool
  default     = false
}

variable "ES_COGNITO_USER_POOL_ID" {
  description = "ID of the Cognito User Pool to use"
  type        = string
  default     = ""
}

variable "ES_COGNITO_IDENTITY_POOL_ID" {
  description = "ID of the Cognito Identity Pool to use"
  type        = string
  default     = ""
}

variable "ES_COGNITO_ROLE_ARN" {
  description = "ARN of the IAM role that has the AmazonESCognitoAccess policy attached"
  type        = string
  default     = ""
}

variable "ES_CREATE_SERVICE_ROLE" {
  description = "Create service link role for AWS Elasticsearch Service"
  type        = bool
  default     = true
}

variable "LOG_GROUP_NAME" {
  description = "Cloudwatch log group name"
  type        = string
  default     = ""
}

variable "LOG_STREAM_NAME" {
  description = "Cloudwatch log stream name"
  type        = string
  default     = ""
}

variable "LOG_GROUP_RETENTION" {
  description = "Log group retention in days"
  type        = number
}

variable "COGNITO_API_KEY" {
  type        = string
  default     = ""
  description = "App api key for integration with corporate cognito"
}

variable "DELEGATION_ZONE_ID" {
  description = "Zone to which project zone should be delegated"
  type        = string
}

variable "LOGS_SOURCE_ACCOUNTS" {
  description = "List of ID's of other accounts which forwarding logs to ES"
  type        = list(string)
  default     = []
}