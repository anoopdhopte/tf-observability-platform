resource "random_string" "id" {
  length  = 4
  special = false
  upper   = true
  lower   = false
  number  = false
}

module "common" {
  source                = "git::https://bitbucket.org/psi-sta-digitalid/common.git//modules?ref=1.2.1"
  ACCOUNT_NAME          = var.ACCOUNT_NAME
  AWS_REGION            = var.AWS_REGION
  ENVIRONMENT           = var.ENVIRONMENT
  PROJECT               = var.PROJECT
  KMS_SHARED_AMI_KEY_ID = var.KMS_SHARED_AMI_KEY_ID
  AMI_OWNER             = var.AMI_OWNER
}

module "encryption" {
  source              = "git::https://bitbucket.org/psi-sta-digitalid/encryption.git//modules?ref=1.0.4"
  globals             = module.common.globals
  KMS_KEY_NAME_SUFFIX = random_string.id.result
}

module "dns_zone" {
  source                  = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/zone_public?ref=1.0.3"
  globals                 = module.common.globals
  DNS_ZONE_NAME           = var.DNS_ZONE_NAME
  CREATE_ZONE_CERTIFICATE = true
}

module "private_dns_zone" {
  source          = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/zone_private?ref=1.1.0"
  globals         = module.common.globals
  networking      = module.networking.common
  DNS_ZONE_NAME   = var.DNS_ZONE_NAME
  ADDITIONAL_VPCS = var.PRIVATE_ZONE_ADDITIONAL_VPCS
}

resource "aws_route53_zone_association" "vpc_zone_association" {
  count   = length(var.ADDITIONAL_ASSOCIATED_ZONES)
  vpc_id  = module.networking.vpc_id
  zone_id = var.ADDITIONAL_ASSOCIATED_ZONES[count.index]
}

module "networking" {
  source                  = "git::https://bitbucket.org/psi-sta-digitalid/vpc.git//modules/networking?ref=2.1.1"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = var.SINGLE_NAT_GATEWAY
  CREATE_PRIVATE_DNS_ZONE = false
  FLOW_LOGS_ENABLED       = var.FLOW_LOGS_ENABLED
  FLOW_LOGS_BUCKET_ARN    = module.logs_bucket.s3_bucket_arn
  FLOW_LOGS_TRAFFIC_TYPE  = var.FLOW_LOGS_TRAFFIC_TYPE  
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
  debug = true
}

module "ingress_alb" {
  source                     = "git::https://bitbucket.org/psi-sta-digitalid/ingress.git//modules/alb?ref=1.0.9"
  globals                    = module.common.globals
  networking                 = module.networking.common
  PUBLIC_SUBNET_IDS          = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN   = module.dns_zone.wildcard_certificate_arn
  INGRESS_ACCESS_CIDR        = concat(var.OFFICE_CIDR_BLOCKS,var.CI_CIDR_BLOCKS,var.EXTRA_CIDR_BLOCKS)
  INTERNAL                   = false
  ENABLE_DELETION_PROTECTION = var.ENABLE_DELETION_PROTECTION
}

module "waf" {
  source                = "git::https://bitbucket.org/psi-sta-digitalid/waf.git//modules/regional?ref=1.0.3"
  globals               = module.common.globals
  WAF_PREFIX            = "${var.PROJECT}-${var.ENVIRONMENT}"
  ALB_ARN               = [module.ingress_alb.ingress_arn]
  S3_ACCESS_LOGS_BUCKET = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : null
  MFA_DELETE            = var.MFA_ENABLED
}

module "local_ingress_alb_eks" {
  source                     = "git::https://bitbucket.org/psi-sta-digitalid/ingress.git//modules/alb?ref=1.0.9"
  globals                    = module.common.globals
  networking                 = module.networking.common
  PUBLIC_SUBNET_IDS          = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN   = module.dns_zone.wildcard_certificate_arn
  INTERNAL                   = true
  ENABLE_DELETION_PROTECTION = var.ENABLE_DELETION_PROTECTION
}

module "eks" {
  source              = "git::https://bitbucket.org/psi-sta-digitalid/eks.git//modules/cluster?ref=2.0.9"
  globals             = module.common.globals
  networking          = module.networking.common
  SUBNET_IDS          = module.networking.application_subnet_ids
  KMS_KEY_ID          = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS = concat(var.OFFICE_CIDR_BLOCKS,var.CI_CIDR_BLOCKS,var.EXTRA_CIDR_BLOCKS)
  ADMIN_ROLES_ARN     = var.ADMIN_ROLES_ARN
  CREATE_KUBECONFIG   = true
  KUBERNETES_VERSION  = var.KUBERNETES_VERSION
}

module "eks_nodes" {
  source             = "git::https://bitbucket.org/psi-sta-digitalid/eks.git//modules/node?ref=2.0.9"
  globals            = module.common.globals
  cluster            = module.eks.cluster
  KMS_KEY_ID         = module.encryption.kms_key_id
  SUBNET_IDS         = module.networking.application_subnet_ids
  KUBERNETES_VERSION = var.KUBERNETES_VERSION
  INSTANCE_TYPE      = var.INSTANCE_TYPE
  SECURITY_GROUPS_ID = [
    module.networking.default_security_group_id,
    module.ingress_alb.ingress_to_nodes_security_group,
    module.local_ingress_alb_eks.ingress_to_nodes_security_group
  ]
  TARGET_GROUP_ARNS           = [module.ingress_alb.ingress_80_arn, module.local_ingress_alb_eks.ingress_80_arn]
  DOCKER_REGISTRY_CREDENTIALS = var.DOCKER_REGISTRY_CREDENTIALS
  ADDITIONAL_SSH_KEYS         = var.ADDITIONAL_SSH_KEYS
}

module "ssm" {
  source    = "git::https://bitbucket.org/psi-sta-digitalid/iam.git//modules/role_policy_attachment?ref=1.0.1"
  ROLE_NAME = module.eks.node_role_name
  POLICIES  = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

module "nginx_ingress" {
  source = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/nginx-ingress?ref=1.9.4"
}

module "prometheus_monitoring" {
  source                       = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/prometheus?ref=1.10.5"
  globals                      = module.common.globals
  CHART_VERSION                = var.CHART_VERSION
  GRAFANA_ENABLED              = true
  GRAFANA_VERSION              = "7.3.7"
  GRAFANA_INGRESS              = true
  GRAFANA_INGRESS_ADDRESS      = "grafana.${var.DNS_ZONE_NAME}"
  GRAFANA_INGRESS_WHITELIST    = concat(var.OFFICE_CIDR_BLOCKS,var.CI_CIDR_BLOCKS,var.EXTRA_CIDR_BLOCKS)
  GRAFANA_COGNITO_ENABLED      = true
  GRAFANA_DISABLE_LOGIN_FORM   = false
  GRAFANA_APP_CLIENT_ID        = aws_cognito_user_pool_client.grafana_app_client.id
  GRAFANA_APP_CLIENT_SECRET    = aws_cognito_user_pool_client.grafana_app_client.client_secret
  GRAFANA_AUTH_URL             = join("", ["https://", aws_cognito_user_pool_domain.cognito_domain_name.domain, "/oauth2/authorize"])
  GRAFANA_TOKEN_URL            = join("", ["https://", aws_cognito_user_pool_domain.cognito_domain_name.domain, "/oauth2/token"])
  GRAFANA_API_URL              = join("", ["https://", aws_cognito_user_pool_domain.cognito_domain_name.domain, "/oauth2/userInfo"])
  PROMETHEUS_INGRESS           = true
  PROMETHEUS_INGRESS_ADDRESS   = "prometheus.${var.DNS_ZONE_NAME}"
  PROMETHEUS_INGRESS_WHITELIST = var.PROMETHEUS_INGRESS_WHITELIST
  ALERTMANAGER_INGRESS_ADDRESS = "alertmanager.${var.DNS_ZONE_NAME}"
  ZONE_ID                      = module.dns_zone.zone_id
  ALIAS_NAME                   = module.ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID                = module.ingress_alb.ingress_zone_id
  PROMETHEUS_EXTERNAL_LABEL    = var.PROMETHEUS_EXTERNAL_LABEL
  DEPENDS_ON_MODULE            = [module.eks]
}

module "grafana_dashboards" {
  source     = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/grafana-dashboards?ref=1.10.5"
  DASHBOARDS = var.GRAFANA_DASHBOARDS
}

module "grafana_datasource" {
  source = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/grafana-datasource?ref=1.10.5"
  DATASOURCES = var.GRAFANA_DATASOURCES
}

module "thanos_receiver" {
  source                = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/thanos-receiver?ref=1.9.8"
  globals               = module.common.globals
  DNS_ZONE_SUFFIX       = var.DNS_ZONE_NAME
  KMS_KEY_ID            = module.encryption.kms_key_id
  ROLE_NAME             = module.eks.node_role_name
  BUCKET_FORCE_DESTROY  = false
  S3_ACCESS_LOGS_BUCKET = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : null
  S3_MFA_DELETE         = var.MFA_ENABLED
}

module "metrics_server" {
  source = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/metric-server?ref=1.9.8"
}

module "registry" {
  source                          = "git::https://bitbucket.org/psi-sta-digitalid/kubernetes.git//modules/docker-registry?ref=1.10.8"
  globals                         = module.common.globals
  KMS_KEY_ID                      = module.encryption.kms_key_id
  ROLE_NAME                       = module.eks.node_role_name
  DOCKER_RELEASE_INGRESS_ADDRESS  = "docker-release.${var.DNS_ZONE_NAME}"
  DOCKER_SNAPSHOT_INGRESS_ADDRESS = "docker-snapshot.${var.DNS_ZONE_NAME}"
  DOCKER_SNAPSHOT                 = true
  S3_ACCESS_LOGS_BUCKET           = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : null
  S3_MFA_DELETE                   = var.MFA_ENABLED
}

module "internal_addons_aliases" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/alias?ref=1.0.5"
  ZONE_ID       = module.private_dns_zone.zone_id
  ALIAS_NAME    = module.local_ingress_alb_eks.ingress_dns_name
  ALIAS_ZONE_ID = module.local_ingress_alb_eks.ingress_zone_id
  DOMAIN_NAMES  = concat(["thanos-receiver.${var.DNS_ZONE_NAME}", "docker-release.${var.DNS_ZONE_NAME}", "docker-snapshot.${var.DNS_ZONE_NAME}"], var.EXTRA_INTERNAL_DNS_ALIASES)
}
