output "dns_public_name_servers" {
  description = "Return public name servers"
  value       = module.dns_zone.public_name_servers
}

output "dns_zone_id" {
  description = "Return public zone id"
  value       = module.dns_zone.zone_id
}

output "dns_zone_name" {
  description = "Return public zone name"
  value       = module.dns_zone.zone_name
}

output "common_globals" {
  description = "Return common globals"
  value       = module.common.globals
}

output "eks_name" {
  description = "Return name of eks cluster"
  value       = module.eks.name
}

output "eks_cluster" {
  description = "Return common networking variables used by other modules"
  value       = "module.eks.cluster"
}

output "eks_endpoint" {
  description = "Return endpoint of eks cluster"
  value       = module.eks.endpoint
}

output "eks_kubeconfig_filename" {
  description = "Return eks kubeconfig filename"
  value       = module.eks.kubeconfig_filename
}

output "eks_node_role_arn" {
  description = "Return eks nodes arn"
  value       = module.eks.node_role_arn
}

output "networking_common" {
  description = "Return networking globals"
  value       = module.networking.common
}

output "networking_public_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.public_route_table_ids
}

output "networking_public_subnets" {
  description = "Return public subnets"
  value       = module.networking.public_subnets
}

output "networking_public_subnet_ids" {
  description = "Return public subnets id"
  value       = module.networking.public_subnet_ids
}

output "networking_application_subnets" {
  description = "Return application subnets"
  value       = module.networking.application_subnets
}

output "networking_application_subnet_ids" {
  description = "Return application subnets id"
  value       = module.networking.application_subnet_ids
}

output "networking_application_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.application_route_table_ids
}

output "networking_data_route_table_ids" {
  description = "Return networking data_route_table_ids"
  value       = module.networking.data_route_table_ids
}

output "identity_provider_name" {
  description = "Return name of identity provider"
  value       = aws_cognito_identity_provider.ad_provider.provider_name
}

output "kubernetes_cluster_endpoint" {
  description = "Return EKS cluster endpoint"
  value       = data.aws_eks_cluster.cluster.endpoint
}

output "kubernetes_cluster_ca_certificate" {
  description = "Return EKS cluster certificate"
  value       = data.aws_eks_cluster.cluster.certificate_authority[0].data
}

output "kubernetes_cluster_token" {
  description = "Return EKS cluster token"
  value       = data.aws_eks_cluster_auth.cluster.token
}
