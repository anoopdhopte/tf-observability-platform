module "logs_bucket" {
  source          = "git::https://bitbucket.org/psi-sta-digitalid/s3.git//modules/logs?ref=1.0.4"
  globals         = module.common.globals
  BUCKET_NAME     = "-${var.AWS_REGION}"
  EXPIRATION_DAYS = "30"
  SSE_ALGORITHM   = "AES256"
  MFA_DELETE      = var.MFA_ENABLED
}

module "logging_elasticsearch_role" {
  source             = "git::https://bitbucket.org/psi-sta-digitalid/iam.git//modules/role?ref=1.0.2"
  globals            = module.common.globals
  NAME               = "${var.ENVIRONMENT}-${var.PROJECT}-${var.AWS_REGION}-elasticsearch-logging-role"
  DESCRIPTION        = "Role assumed by other accounts to push logs"
  AWS_PRINCIPALS     = var.LOGS_SOURCE_ACCOUNTS
  SERVICE_PRINCIPALS = ["es.amazonaws.com"]
}

resource "aws_iam_policy" "elasticsearch_logging_policy" {
  name        = "${var.ENVIRONMENT}-${var.PROJECT}-${var.AWS_REGION}-logging-policy"
  description = "Logging elasticsearch policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": "es:*",
            "Resource": "${module.elasticsearch.arn}/*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "es_logging_policy_attach" {
  role       = module.logging_elasticsearch_role.name
  policy_arn = aws_iam_policy.elasticsearch_logging_policy.arn
}
