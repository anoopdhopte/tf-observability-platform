##
# this part should be used only in teribu test environment
##
module "delegation" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/delegation?ref=1.0.3"
  NAME_SERVERS  = module.dns_zone.public_name_servers
  DNS_ZONE_NAME = module.dns_zone.zone_name
  ZONE_ID       = var.DELEGATION_ZONE_ID
}