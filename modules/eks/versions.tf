terraform {
  required_version = ">= 0.12.0"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    # Update it only if changes in this module really required features provided by this version
    # Don't update blindly as we will use latest anyway.
    null       = ">= 2.1.2"
    local      = ">= 1.4"
    kubernetes = "= 2.2.0"
    aws        = "<3.37"
    helm       = ">= 1.0.0"
  }
}
