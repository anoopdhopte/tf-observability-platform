provider "aws" {
  region = var.AWS_REGION
  assume_role {
    role_arn     = var.AWS_ASSUME_ROLE
    session_name = "observability-platform"
  }
}

provider "aws" {
  region = "us-east-1"
  alias  = "certificates"
  assume_role {
    role_arn     = var.AWS_ASSUME_ROLE
    session_name = "observability-platform"
  }
}