variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
}

variable "AWS_ASSUME_ROLE" {
  description = "Role to which provider should be assumed"
  default     = ""
  type        = string
}

variable "PROJECT" {
  description = "Provide name of the project"
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "testing"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "OFFICE_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of lodz office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "CI_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of CI"
  default     = ["18.192.228.102/32", "18.158.189.113/32", "18.192.153.221/32", "18.193.144.231/32"]
  type        = list(string)
}

variable "EXTRA_CIDR_BLOCKS" {
  description = "Provide extra CIDR blocks"
  default     = []
  type        = list(string)
}

variable "SINGLE_NAT_GATEWAY" {
  default     = true
  description = "Define whether only single nat gateway should be created for prod envs should be false"
  type        = bool
}

variable "FLOW_LOGS_ENABLED" {
  default     = false
  description = "Define whether vpc flow logs should be enabled"
  type        = bool
}

variable "FLOW_LOGS_TRAFFIC_TYPE" {
  default     = "REJECT"
  description = "Provide type of vpc traffic to capture (ACCEPT, REJECT, ALL)"
  type        = string
}

variable "DELEGATION_ZONE_ID" {
  description = "Zone to which project zone should be delegated"
  type        = string
}

variable "ADMIN_ROLES_ARN" {
  description = "Roles which should have access to kubernetes API"
  type        = list(string)
}

variable "PROMETHEUS_EXTERNAL_LABEL" {
  description = "Prometheus label used to thanos connection"
  type        = string
}

variable "ALERTMANAGER_CONFIG" {
  type        = string
  description = "Alertmanager custom config. Overrides defaults from prometheus-stack https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml#L154"
  default     = ""
}

variable "KUBERNETES_VERSION" {
  description = "Version of kubernetes which should be deployed"
  type        = string
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "Provide additional ssh public keys"
  default     = []
  type        = list(string)
}

variable "KMS_SHARED_AMI_KEY_ID" {
  description = "KMS key used for shared images"
  type        = string
  default     = ""
}

variable "AMI_OWNER" {
  description = "Provide ami owner"
  type        = string
  default     = ""
}

variable "PROMETHEUS_INGRESS_WHITELIST" {
  description = "Provide whitelist for prometheus"
  type        = list(string)
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
}

variable "GRAFANA_DASHBOARDS" {
  description = "List of custom Dashboards to load"
  type        = map(any)
  default     = {}
}

variable "INSTANCE_TYPE" {
  description = "Provide kubernetes node instance type"
  default     = "t3.medium"
  type        = string
}

## Elasticsearch env's

variable "ES_VERSION" {
  description = "Elasticsearch version"
  type        = string
  default     = "7.7"
}

variable "ES_NODE_TO_NODE_ENCRYPTION" {
  description = "Whether to enable node-to-node encryption"
  type        = bool
  default     = true
}
variable "ES_ADVACED_SECURITY_ENABLED" {
  description = "Enable advance security option"
  type        = bool
  default     = true
}

variable "ES_INTERNAL_DATABASE_ENABLED" {
  description = "Create internal Kibana user database"
  type        = bool
  default     = false
}

variable "ES_MASTER_USER_ARN" {
  description = "Master user arn"
  type        = string
  default     = ""
}

variable "ES_MASTER_USER_NAME" {
  description = "Master user name"
  type        = string
  default     = null
}

variable "ES_MASTER_USER_PASS" {
  description = "Master user name"
  type        = string
  default     = null
}

variable "ES_INSTANCE_TYPE" {
  description = "Instance type for ES nodes"
  type        = string
  default     = "r5.large.elasticsearch"
}

variable "ES_INSTANCE_COUNT" {
  description = "Number of instances in the cluster"
  type        = number
  default     = 3
}

variable "ES_MASTER_ENABLED" {
  description = "Indicates whether to use a dedicated master node for the Amazon ES domain"
  type        = bool
  default     = true
}

variable "ES_MASTER_COUNT" {
  description = "The number of instances to use for the master node"
  type        = number
  default     = 3
}

variable "ES_MASTER_TYPE" {
  description = "The hardware configuration of the computer that hosts the dedicated master node"
  type        = string
  default     = "r5.large.elasticsearch"
}

variable "ES_ZONE_AWARENESS" {
  description = "Indicates whether to enable zone awareness for the Amazon ES domain"
  type        = bool
  default     = false
}

variable "ES_ZONE_COUNT" {
  description = "Number of Availability Zones for the domain (Use only if Awareness is enabled)"
  type        = number
  default     = 2
}

variable "ES_ENCRYPT_AT_REST" {
  description = "Enable encryption at rest (only specific instance family types support it: m4, c4, r4, i2, i3 default: false)"
  type        = bool
  default     = false
}

variable "ES_KMS_KEY" {
  description = "KMS key id for elasticsearch"
  type        = string
  default     = ""
}

variable "EBS_ENABLED" {
  description = "Whether EBS volumes are attached to data nodes in the domain"
  type        = bool
  default     = true
}

variable "EBS_VOLUME_SIZE" {
  description = "The size (in GiB) of the EBS volume for each data node. Required if ebs_enabled is set to true"
  type        = number
  default     = 10
}

variable "EBS_VOLUME_TYPE" {
  description = "The EBS volume type to use with the Amazon ES domain, such as standard, gp2, io1, st1, or sc1"
  type        = string
  default     = "gp2"
}

variable "EBS_IOPS" {
  description = "The number of I/O operations per second (IOPS) that the volume supports. This property applies only to the Provisioned IOPS (SSD) EBS volume type"
  type        = number
  default     = 0
}

variable "ES_DOMAIN_ENFORCE_HTTPS" {
  description = "Whether or not to require HTTPS"
  type        = bool
  default     = false
}

variable "ES_DOMAIN_SECURITY_POLICY" {
  description = "The name of the TLS security policy that needs to be applied to the HTTPS endpoint. Valid values: `Policy-Min-TLS-1-0-2019-07` and `Policy-Min-TLS-1-2-2019-07`"
  type        = string
  default     = "Policy-Min-TLS-1-2-2019-07"
}

variable "ES_SG_ID" {
  description = "List of VPC Security Group IDs to be applied to the Elasticsearch domain endpoints. If omitted, the default Security Group for the VPC will be used"
  type        = list(any)
  default     = []
}

variable "ES_SUBNET_ID" {
  description = "List of VPC Subnet IDs for the Elasticsearch domain endpoints to be created in"
  type        = list(any)
  default     = []
}

variable "ES_VPC_ACCESS" {
  description = "Set to true to run ES domain in VPC access mode"
  type        = bool
  default     = false
}

variable "ES_LOGS_ENABLED" {
  description = "Specifies whether given log publishing option is enabled or not"
  type        = bool
  default     = false
}

variable "ES_LOG_TYPE" {
  description = "A type of Elasticsearch log. Valid values: INDEX_SLOW_LOGS, SEARCH_SLOW_LOGS, ES_APPLICATION_LOGS"
  type        = string
  default     = "INDEX_SLOW_LOGS"
}

variable "ES_CLOUDWATCH_LOG_GROUP" {
  description = "ARN of the Cloudwatch log group to which log needs to be published"
  type        = string
  default     = ""
}

variable "ES_COGNITO_ENABLED" {
  description = "Specifies whether Amazon Cognito authentication with Kibana is enabled or not"
  type        = bool
  default     = false
}

variable "ES_COGNITO_USER_POOL_ID" {
  description = "ID of the Cognito User Pool to use"
  type        = string
  default     = ""
}

variable "ES_COGNITO_IDENTITY_POOL_ID" {
  description = "ID of the Cognito Identity Pool to use"
  type        = string
  default     = ""
}

variable "ES_COGNITO_ROLE_ARN" {
  description = "ARN of the IAM role that has the AmazonESCognitoAccess policy attached"
  type        = string
  default     = ""
}

variable "ES_CREATE_SERVICE_ROLE" {
  description = "Create service link role for AWS Elasticsearch Service"
  type        = bool
  default     = true
}

variable "LOG_GROUP_NAME" {
  description = "Cloudwatch log group name"
  type        = string
  default     = ""
}

variable "LOG_STREAM_NAME" {
  description = "Cloudwatch log stream name"
  type        = string
  default     = ""
}

variable "LOG_GROUP_RETENTION" {
  description = "Log group retention in days"
  type        = number
}

variable "COGNITO_API_KEY" {
  type        = string
  default     = ""
  description = "App api key for integration with corporate cognito"
}

variable "COGNITO_API_SECRET" {
  type        = string
  default     = ""
  description = "App api key secret for integration with corporate cognito"
}

variable "FEDERATED_COGNITO_DOMAIN" {
  type        = string
  default     = ""
  description = "Federated cognito public url"
}

variable "USER_POOL_ID" {
  type        = string
  default     = ""
  description = "User pool id"
}

variable "ISSUER_URL" {
  type        = string
  default     = ""
  description = "Issuer url"
}

variable "LOGS_SOURCE_ACCOUNTS" {
  description = "List of ID's of other accounts which forwarding logs to ES"
  type        = list(string)
  default     = []
}

variable "DOCKER_REGISTRY_CREDENTIALS" {
  description = "Provide list of docker registries with credentials."
  default     = []
  type        = list(string)
}

variable "ADDITIONAL_CLIENTS" {
  default     = []
  type        = list(any)
  description = "A list of additional app clients for a user pool. It supports the same values as a normal app resource"
}

variable "ES_WARM_ENABLED" {
  description = "Indicates whether to enable warm storage"
  type        = bool
  default     = false
}

variable "ES_WARM_COUNT" {
  description = "The number of warm nodes in the cluster. Valid values are between 2 and 150"
  type        = number
  default     = 2
}

variable "ES_WARM_TYPE" {
  description = "The instance type for the Elasticsearch cluster's warm nodes. Valid values are ultrawarm1.medium.elasticsearch, ultrawarm1.large.elasticsearch and ultrawarm1.xlarge.elasticsearch"
  type        = string
  default     = "ultrawarm1.medium.elasticsearch"
}

variable "CHART_VERSION" {
  default     = "14.5.0"
  type        = string
  description = "Prometheus chart version, see https://github.com/prometheus-community/helm-charts/releases"
}

variable "ENABLE_DELETION_PROTECTION" {
  description = "Enable this will prevent Terraform from deleting the load balancer"
  default     = true
  type        = bool
}

variable "AWS_LOGGING_ENABLED" {
  description = "Enable logging for all AWS components"
  type        = bool
  default     = true
}

variable "MFA_ENABLED" {
  description = "Enable multi-factor authentication for all AWS components"
  type        = bool
  default     = false
}

### NESSUS
variable "NESSUS_KEY" {
  description = "Key used by nessus to authenticate"
  type        = string
  default     = "35e4c0a85554facf6e7c0a74cc43e54141dfe5da94d51273b26460df3a54547c"
}

variable "NESSUS_INSTANCE_TYPE" {
  description = "Type of ec2 instance used to setup nessus"
  type        = string
  default     = "t3.2xlarge"
}

variable "NESSUS_SG_EGRESS_RULES" {
  description = "List of addresses where nessus should have access"
  type        = list(string)
  default     = ["54.93.254.128/26:443", "18.194.95.64/26:443", "3.124.123.128/25:443", "3.67.7.128/25:443", "35.177.219.0/26:443", "3.9.159.128/25:443", "18.168.180.128/25:443", "18.168.224.128/25:443", "35.177.219.11/32:443", "35.177.219.10/32:443", "162.159.130.83/32:443", "162.159.129.83/32:443"]
}

variable "ENABLE_NESSUS" {
  description = "Boolean value, if set to true starts Nessus on environment"
  type        = bool
  default     = false
}

variable "GRAFANA_DATASOURCES" {
  description = "List of custom Grafana datasources to load"
  type        = map(any)
  default     = {}
}

variable "PRIVATE_ZONE_ADDITIONAL_VPCS" {
  default     = []
  description = "Additional VPCs authorized to associate with private DNS zone"
  type = list(object({
    id     = string,
    region = string
  }))
}

variable "ADDITIONAL_ASSOCIATED_ZONES" {
  default     = []
  description = "IDs of hosted zones which VPC will be associated with"
  type        = list(string)
}

variable "EXTRA_INTERNAL_DNS_ALIASES" {
  description = "List of extra aliases which should be added to private dns zone which will point to nginx ingress"
  type        = list(string)
  default     = []
}