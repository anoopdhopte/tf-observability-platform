module "auth_certificate" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/certificates?ref=1.0.3"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "auth.cognito"
  providers = {
    aws = aws.certificates
  }
}

module "authorization_domain_name" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/alias?ref=1.0.3"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = aws_cognito_user_pool_domain.cognito_domain_name.cloudfront_distribution_arn
  ALIAS_ZONE_ID = "Z2FDTNDATAQYW2"
  DOMAIN_NAME   = "auth.cognito.${var.DNS_ZONE_NAME}"
}

module "authorization_domain_name_private_zone" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.private_dns_zone.zone_id
  ALIAS_NAME    = aws_cognito_user_pool_domain.cognito_domain_name.cloudfront_distribution_arn
  ALIAS_ZONE_ID = "Z2FDTNDATAQYW2"
  DOMAIN_NAME   = "auth.cognito.${var.DNS_ZONE_NAME}"
}

resource "aws_cognito_user_pool_domain" "cognito_domain_name" {
  domain          = "auth.cognito.${var.DNS_ZONE_NAME}"
  certificate_arn = module.auth_certificate.certificate_arn
  user_pool_id    = module.cognito_user_pool.id
  depends_on      = [module.cognito_alias]
}

module "cognito_alias" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/alias?ref=1.0.3"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = module.ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID = module.ingress_alb.ingress_zone_id
  DOMAIN_NAME   = "cognito.${var.DNS_ZONE_NAME}"
}

module "cognito_alias_private_zone" {
  source        = "git::https://bitbucket.org/psi-sta-digitalid/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.private_dns_zone.zone_id
  ALIAS_NAME    = module.local_ingress_alb_eks.ingress_dns_name
  ALIAS_ZONE_ID = module.local_ingress_alb_eks.ingress_zone_id
  DOMAIN_NAME   = "cognito.${var.DNS_ZONE_NAME}"
}

module "cognito_user_pool" {
  source                       = "git::https://bitbucket.org/psi-sta-digitalid/cognito.git//modules/user_pool?ref=1.2.0"
  globals                      = module.common.globals
  NAME                         = "${var.ENVIRONMENT}-${var.PROJECT}"
  RESOURCE_SERVER_IDENTIFIER   = "${var.ENVIRONMENT}-${var.PROJECT}"
  USERNAME_ATTRIBUTES          = ["email"]
  ALLOW_ADMIN_CREATE_USER_ONLY = true
  CASE_SENSITIVE               = null

  ADDITIONAL_CLIENTS = var.ADDITIONAL_CLIENTS

  SCHEMA = [{
    attribute_data_type      = "String"
    mutable                  = true
    name                     = "email"
    required                 = true
    developer_only_attribute = false
    string_attribute_constraints = [{
      max_length = 2048
      min_length = 0
    }]
    },
    {
      name                     = "given_name"
      attribute_data_type      = "String"
      mutable                  = true
      required                 = true
      developer_only_attribute = false
      string_attribute_constraints = [{
        max_length = 2048
        min_length = 0
      }]
    },
    {
      name                     = "family_name"
      attribute_data_type      = "String"
      mutable                  = true
      required                 = true
      developer_only_attribute = false
      string_attribute_constraints = [{
        max_length = 2048
        min_length = 0
      }]
    }
  ]
}

resource "aws_cognito_user_pool_client" "kibana_app_client" {
  name                                 = "kibana"
  user_pool_id                         = module.cognito_user_pool.id
  supported_identity_providers         = [aws_cognito_identity_provider.ad_provider.provider_name]
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  callback_urls                        = ["https://elk.${var.DNS_ZONE_NAME}"]
  logout_urls                          = ["https://elk.${var.DNS_ZONE_NAME}"]
  prevent_user_existence_errors        = "ENABLED"
  refresh_token_validity               = 7
  allowed_oauth_flows_user_pool_client = true
  generate_secret                      = true
  depends_on                           = [aws_cognito_identity_provider.ad_provider]
}

resource "aws_cognito_user_pool_client" "grafana_app_client" {
  name                                 = "grafana"
  user_pool_id                         = module.cognito_user_pool.id
  supported_identity_providers         = [aws_cognito_identity_provider.ad_provider.provider_name]
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  callback_urls                        = ["https://grafana.${var.DNS_ZONE_NAME}/login/generic_oauth"]
  logout_urls                          = ["https://grafana.${var.DNS_ZONE_NAME}/login/generic_oauth"]
  prevent_user_existence_errors        = "ENABLED"
  refresh_token_validity               = 7
  allowed_oauth_flows_user_pool_client = true
  generate_secret                      = true
  depends_on                           = [aws_cognito_identity_provider.ad_provider]
}

resource "aws_cognito_identity_provider" "ad_provider" {
  user_pool_id  = module.cognito_user_pool.id
  provider_name = "idemia"
  provider_type = "OIDC"
  depends_on    = [module.cognito_user_pool]

  provider_details = {
    authorize_scopes          = "openid"
    client_id                 = var.COGNITO_API_KEY
    client_secret             = var.COGNITO_API_SECRET
    attributes_request_method = "GET"
    oidc_issuer               = "${var.ISSUER_URL}/${var.USER_POOL_ID}"
    authorize_url             = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/authorize"
    token_url                 = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/token"
    attributes_url            = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/userInfo"
    jwks_uri                  = "${var.ISSUER_URL}/${var.USER_POOL_ID}/.well-known/jwks.json"
  }

  attribute_mapping = {
    email              = "email"
    family_name        = "family_name"
    given_name         = "given_name"
    username           = "sub"
    preferred_username = "email"
    name               = "name"
  }
}

module "identity_pool" {
  source            = "git::https://bitbucket.org/psi-sta-digitalid/cognito.git//modules/identity_pool?ref=1.2.0"
  globals           = module.common.globals
  NAME              = "${var.ENVIRONMENT}_${var.PROJECT}"
  PROVIDER_NAME     = module.cognito_user_pool.endpoint
  CLIENT_ID         = aws_cognito_user_pool_client.kibana_app_client.id
  DEPENDS_ON_MODULE = module.cognito_user_pool
}
