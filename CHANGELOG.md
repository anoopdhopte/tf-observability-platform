# Change Log
-------------
# 1.5.2 2021-11-15
Features:
- Refactor CIDR blocks
- Update modules/eks/output.tf

# 1.5.1 2021-11-03
Features:
- GID-2042 - Update docker-registry version
- SIE-971  - update nessus scanner version

# 1.5.0 2021-09-23
Features:
  - GID-2232 - Add flow logs to vpc
  - GID-2232 - Add possibility to use multiple NAT GATEWAYS

## 1.4.1 2021-09-22
Fixes:
  - Fix CVE issues in k8s-sidecar image

## 1.4.0 2021-09-22
Features:
  - new common module
  - Add support for DNS peering
Fixes:
  - KMS key managed by user to Elasticsearch domain
  - SG rules for Nessus scanner 


## 1.3.1 2021-08-12
Features:
  - New variable AWS_ASSUME_ROLE which gives possibility to use another Terraform role during apply
## 1.3.0 2021-08-09
Features:
  - Nessus scanner as optional element of platform
New variables:
  - ADDITIONAL_SSH_KEYS - Additional ssh keys which can be added to kubernetes nodes
  - ENABLE_NESSUS - Boolean value which starts or stops nessus scanner
  - 3 more variables specific for Nessus setup - more info in Readme
## 1.2.1 2021-07-26
Fixes:
  - Prometheus pull image fix
## 1.2.0 2021-06-28
Features:
   - New variable LOGS_SOURCE_ACCOUNTS where operator need to put all accounts which should have possibility to push logs 
   - Logs are sent directly from fluentbit to elastic instead of fluentbit → lambda → elastic
   - There is possiblity to create elastic indexes for each app like: app-op-dev-fluent-bit-2021.06.28
## 1.1.6 2021-06-16
Features:
   - Update version of thanos-receiver and docker-registry to 1.9.8
   - Grafana dashboards by variable
   - Terraform Kubernetes provider version 2.2.0

Fixes:
   - Fix name of access logs bucket and MFA enabled
   - Delete redundant property in logs bucket
## 1.1.5 2021-05-07
Features:
   - Update version of prometheus module to 1.9.4 and use versioning in helm chart
   - all submodules from official release tags
   - all submodules in newest version
## 1.1.4 2021-04-13
Features:
   - All terraform submodules in newest versions
   - Prometheus repo restored to official
## 1.1.3 2021-03-06
Features:
   - UltraWarm instances for Elasticsearch
   - Prometheus charts from Idemia Artifactory repo
   - Grafana update to 7.3.7

## 1.1.2 2021-03-11
Features:
   - Added posibility to add cognito clients for app platforms
   - Add output with details about cognito app clients

## 1.0.2 2021-03-03
Fixes:
   - Docker-registry in eu-north-1 region
   - syntax interpolations

## 1.0.1 2021-02-23
Features:
   - docker registry
   - kibana domain cname

## 1.0.0 2021-02-09
Features:
   - vpc
   - external alb
   - internal alb (for vpc peering)
   - thanos monitoring
   - grafana
   - elasticsearch
   - cognito


## 0.1.0 2020-10-20
Initial package

#### Added
- Initial package for observability-platform
